To use: 

0. run pip freeze > requirements.txt
This generates the file which can be used to pack the required packages.

1. Run ```python pack.py in the depends directory``` 
1a. This should generate a folder called "wheelhouse"
1b. This should also generate a dill.pkl file (which contain binary from all wheelhouse). 

2. Upload entire depends package source code. 
Use the unpack.py or run its content to install the packages. 

MAKE SURE you include the class SimpleObject in both the pack and unpack stage as that is part of the data structure encoded
into the pickle data.  
